import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please type your name below:");
        String name = sc.nextLine();
        greetings(name);
        System.out.println("Please input integer n. Where n is the size of a sequence of integers:");
        int n = sc.nextInt();
        sequence(sc, n);
        sc.close();
    }

    private static void greetings(String name) {
        if (name.equals("")) {
            System.out.println("Incorrect input");
        } else {
            System.out.println("Hello and welcome " + name + "!");
        }
    }

    private static void sequence(Scanner sc, int n) {
        int[] array = new int[n];
        System.out.println("Enter n-integers in one line divided by space");
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }
        System.out.println("The result of adding a sequence of numbers is " + getAddedResultOfIntegers(array));
        System.out.println("The result of multiplying a sequence of numbers is " + getMultipliedResultOfIntegers(array));
    }

    private static int getMultipliedResultOfIntegers(int[] array) {
        int multiplyResult = 1;
        for (int i = 0; i < array.length; i++){
            multiplyResult *= array[i];
        }
        return multiplyResult;
    }

    private static int getAddedResultOfIntegers(int[] array) {
        int sumResult = 0;
        for (int i = 0; i < array.length; i++){
            sumResult += array[i];
        }
        return sumResult;
    }
}